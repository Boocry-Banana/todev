Todev

Liste des personnes:

	Etienne Gobion 
	Léo Hazard
	Kévin Hego
	Paul Pascal
Description:
	Un projet Todo avec des fonctionnalités de management de projet.

Fonctionnalité:
Écran principal: Il permet d’afficher les tâches sous forme de liste en fonction des projets ou par dates
Création des tâches: Ajout d’un texte, une date, un lieu(Google Maps + itineraire),commentaire, priorité, tag,rappel

Hiérarchie des tâches: possibilité  de créer des sous tâches et de lier les ensemble.

Écran Kanban: Mise en place des tâches sous forme d’un kanban

Écran Calendrier: planning,journée,semaine,mois

Menu sandwich: Productivité, filtre,projet(création partage),paramètre

Paramètre: notification, rappel

Version:
	Nodejs:10.13.0
	expo-CLI:3.8.0
	expo:35.0.0