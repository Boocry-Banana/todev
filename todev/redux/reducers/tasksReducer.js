import { CITIES_ALL } from "../actions/tasksActions";

const INITIAL_STATE = {
    tasks: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CITIES_ALL:

            return { tasks: action.payload };
        default:
            return state;
    }
};