import { createStore, combineReducers, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk'
import serviceReducer from './reducers/serviceReducer';
import tasksReducer from './reducers/tasksReducer';

const reducers = combineReducers({
    services: serviceReducer,
    tasks: tasksReducer
});

export const store = createStore(reducers,applyMiddleware(thunk));