import { AsyncStorage } from 'react-native'
export const TASK_ALL = 'TASK_ALL';

// export const init = (payload) => {
//     return { type: CITIES_ALL, payload };
// };

export const init = () => {
    
    return  (dispatch) => {
        tab = []
        AsyncStorage.getItem('TASKS').then(data => {
            if (data != null) {
                tab = JSON.parse(data);
            }
            console.log(tab)
            dispatch({
                type: TASK_ALL,
                payload:tab
            });
        });
    };  
};

export const add = (task) => {

    return (dispatch) => {
        tab = []
        AsyncStorage.getItem('TASKS').then(data => {
            if (data != null) {
                tab = JSON.parse(data);
            }
            tab.push(task);
            AsyncStorage.setItem('TASKS', JSON.stringify(tab)).then(() => {
                dispatch({
                    type: TASKS_ALL,
                    payload: tab
                });
            });
        });
    };
};

export const del = (task) => {

    return (dispatch) => {
        tab = []
        AsyncStorage.getItem('TASKS').then(data => {
            if (data != null) {
                tab = JSON.parse(data);
            }
            tab.splice(tab.findIndex(e => e === task), 1);
            AsyncStorage.setItem('TASKS', JSON.stringify(tab)).then(() => {
                dispatch({
                    type: TASK_ALL,
                    payload: tab
                });
            });
        });
    };
};

