import axios from 'axios';
const key = 'acc58137-4851-494d-a74a-43344433723f';
const baseUrl = `https://api.navitia.io/v1/coverage`;
// Example of url:`https://api.navitia.io/v1/coverage/fr-idf/journeys?from=2.23807%3B48.89189&to=2.264357%3B48.891408&datetime=20200116T161544&`;

class JourneyService {

    getJourneyService(region = 'fr-idf', fromLong, fromLat, toLong, toLat,datetime ) {
        const url = `${baseUrl}/${region}/journeys?from=${fromLong}%3B${fromLat}&to=${toLong}%3B${toLat}&datetime=${datetime}`
        console.log("url")
        console.log(url)
        return axios.get(url, { headers: { Authorization: key } })
            .then(response => {
                console.log(response.data.journeys[0].section)
                return response.data.journeys[0]
            })
            .catch((error) => {
                console.log('error ' + error);
            });
        
    }
}

export default JourneyService;