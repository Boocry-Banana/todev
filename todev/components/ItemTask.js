import React, { Component } from 'react';
import { View, Button, StyleSheet, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import { SwipeRow } from 'react-native-swipe-list-view';
import { TouchableOpacity } from 'react-native-gesture-handler';

class ItemTask extends Component {
    static propTypes = {
        displayEdit: PropTypes.func.isRequired,
        task: PropTypes.any.isRequired
    }

    state = {}

    
    render() {
        // console.log('777777'+ this.props.tasks);
        return (
            <TouchableOpacity
                style={height= 190,backgroundColor= '#CCC',flexDirection= 'row'}
                onPress={() => this.props.displayEdit(this.props.task)}>
                <View style={styles.standaloneRowFront}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text>{this.props.id}</Text>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Text style={fontSize=8}>N:{this.props.task.name}</Text>
                            <Text style={fontSize=6}>N:{this.props.task.date}</Text>
                            <Text style={fontSize=6}>N:{this.props.task.comments}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default ItemTask;

const styles = StyleSheet.create({
    standaloneRowFront: {
        alignItems: 'center',
        backgroundColor: '#CCC',
        justifyContent: 'center',
        height: 80,
    },
    standaloneRowBack: {
        alignItems: 'center',
        backgroundColor: '#CCC',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 15,
    },
    backTextWhite: {
        color: '#FFF',
    },
    deleteButton: {
        textAlign: 'right',
    },
    
    main_container: {
        height: 190,
        backgroundColor: '#CCC',
        flexDirection: 'row'
    },
    image: {
        width: 120,
        height: 180,
        margin: 5,
    },
    content_container: {
        flex: 1,
        margin: 5
    },
    header_container: {
        flex: 3,
        flexDirection: 'row'
    },
    title_text: {
        fontWeight: 'normal',
        fontSize: 15,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    brand_text: {
        fontWeight: 'bold',
        fontSize: 14,
        color: '#666666'
    },
    description_container: {
        flex: 7
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666'
    },
    date_container: {
        flex: 1
    },
    date_text: {
        textAlign: 'right',
        fontSize: 14
    }
});