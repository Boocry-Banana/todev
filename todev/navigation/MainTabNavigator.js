import React from 'react';
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import HomeScreen from "../screens/HomeScreen";
import { createAppContainer } from "react-navigation";
import Icon from 'react-native-vector-icons/Ionicons';
import CalendarScreen from '../screens/CalendarScreen';
import KanbanScreen from '../screens/KanbanScreen';

const tabNavigator = createMaterialBottomTabNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            tabBarLabel: 'Accueil',
            tabBarIcon: ({ tintColor }) => (
                <Icon color={tintColor} size={25} name={'ios-home'} />
            ),
            barStyle: { backgroundColor: '#0a3d62' }
        }
    },
    calendar: {
        screen: CalendarScreen,
        navigationOptions: {
            tabBarLabel: 'Calendrier',
            tabBarIcon: ({ tintColor }) => (
                <Icon color={tintColor} size={25} name={'ios-calendar'} />
            ),
            barStyle: { backgroundColor: '#0a3d62' }
        }
    },
    // Settings: {
    //     screen: KanbanScreen,
    //     navigationOptions: {
    //         tabBarLabel: 'Kanban',
    //         tabBarIcon: ({ tintColor }) => (
    //             <Icon color={tintColor} size={25} name={'ios-filing'} />
    //         ),
    //         barStyle: { backgroundColor: '#0a3d62' }
    //     }
    // }
},
    {
        initialRouteName: 'Home'
    }
);

export default createAppContainer(tabNavigator);