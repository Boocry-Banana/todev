import React, { Component } from 'react';
import {Platform, Button, FlatList, StyleSheet, Text, View, Image, AsyncStorage, TextInput, SafeAreaView, ScrollView} from 'react-native';
import Loading from '../components/Loading';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { init } from '../redux/actions/tasksActions';
import ItemTask from '../components/ItemTask';
import {Overlay, Avatar } from 'react-native-elements'
import Form from 'react-native-form'
import { Constants, Notifications } from 'expo';
import { Permissions } from 'expo-permissions';

class HomeScreen extends Component {

    state = { data: null, displayCreate: false, isVisible:false, taskToEdit : null, newTask : null };


    handleNotification() {
        console.warn('This is a notification');
    }
    
    componentDidMount = async()=> {
    
        this.getTasks();
        // We need to ask for Notification permissions for ios devices
        let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

        if (Constants.isDevice && result.status === 'granted') {
            console.log('Notification permissions granted.')
        }

        // If we want to do something with the notification when the app
        // is active, we need to listen to notification events and 
        // handle them in a callback
        Notifications.addListener(this.handleNotification);
    }    

    componentDidUpdate(){
        this.getTasks();
    }

    saveTasks = async (tasks) => {
        if (tasks != null) {
            let data = await AsyncStorage.getItem('TASKS');
            let tab = [];
            if (data != null) {
              tab = JSON.parse(data);
            }

            tab = tab.concat(tasks);
            
            console.log(tab);
            await AsyncStorage.setItem('TASKS', JSON.stringify(tab));
        } else {
            alert(`Error n save`);
        }
        const localNotification = {
            title: 'You received a reminder:',
            body: tasks.name
        };

        console.log('localNotification')
        console.log(localNotification)

        const schedulingOptions = {
            time: (tasks.date.getTime())
        }

        console.log('schedulingOptions')
        console.log(schedulingOptions)


        // Notifications show only when app is not active.
        // (ie. another app being used or device's screen is locked)
        Notifications.scheduleLocalNotificationAsync(
            localNotification, schedulingOptions
        );
    }

    writeTasks = async (tasks) => {
        if (tasks != null) {
            let tab = [];
            tab = tab.concat(tasks);
            console.log(tab);
            await AsyncStorage.setItem('TASKS', JSON.stringify(tab));
        } else {
            alert(`Error n save`);
        }
    }

    
    getTasks = async () => {
        let data = await AsyncStorage.getItem('TASKS');
        let tab = [];
        if (data != null) {
            tab = JSON.parse(data);
        }
        this.setState({tasks: tab})
    }
    
    updateDisplayTemp() {
        this.setState({isVisible: true})     
    }
    newButton = ()=>{
        this.setState({displayCreate: true} );
    }
    addTask (){
        const uuidv4 = require('uuid/v4');
        let dict = this.refs.form.getValues()
        let task = {
            id :uuidv4(), // ⇨ random '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed'
            name: dict.taskName,
            date: dict.taskDate,
            comments: dict.taskComments
        }
        console.log(task.name)
        let tab = []
        tab.push(task);
        this.saveTasks(tab);
        this.setState({ isVisible: false, taskToEdit: null })

    }
    deleteTask (taskId){
        this.getTasks();
        console.log(taskId)
        let mTasks = this.state.tasks;
        console.log(mTasks)
        mTasks = mTasks.filter(e => e.id !== taskId); // will return ['A', 'C']
        console.log(mTasks)
        this.writeTasks(mTasks)
        this.setState({ isVisible: false, tasks: mTasks})
        
    }
    _displayEdit = (task) => {
        console.log('_displayEdit1')
        this.setState({isVisible:true})
        this.setState({taskToEdit:task})
    }
    updateTaskToEdit = (attribute, value) => {
        let taskToEdit = this.state.taskToEdit;
        switch(attribute) {
            case "name" : 
            taskToEdit.name = value;
            break
            case "date":
            taskToEdit.date = value;
            break
            case"comments":
            taskToEdit.comments = value;
            break
            default :
            console.log('Error unimplemented') 
        } 
        this.setState(taskToEdit = taskToEdit)
    }

    render() {
        return (
            <SafeAreaView style={styles.droidSafeArea} >
                <ScrollView>
                        {this.state.tasks ? (
                            <View>
                                <FlatList
                                    data={this.state.tasks}
                                    keyExtractor={(item) => item.id}
                                    renderItem={({ item }) => <ItemTask displayEdit={this._displayEdit} task={item} />}
                                    on
                                />
                                <Button title="New task" backgroundColor="black" displayColor="black" onPress={() => this.setState({ isVisible: true, taskToEdit: null })} />
                            </View>
                            ) : (
                                <View><Loading displayColor="orange">
                                    <Image style={{ width: 80, height: 80 }} source={{ uri: `http://openweathermap.org/img/wn/09d.icon}@2x.png` }} />
                                </Loading>
                                </View>
                            )
                        }
                        <Overlay
                            isVisible={this.state.isVisible}
                            windowBackgroundColor="rgba(255, 255, 255, .5)"
                            overlayBackgroundColor="grey"
                            width={300}
                            height='auto'
                            onBackdropPress={() => this.setState({ isVisible: false })}
                        >
                            {this.state.taskToEdit ? (
                                <View>
                                    <Form ref="form">
                                        <Text>Task name</Text>
                                        <TextInput type="TextInput" value={this.state.taskToEdit.name} onChangeText={(text) => this.updateTaskToEdit("name", text)} name="taskName" />
                                        <Text>Task date</Text>
                                        <TextInput type="TextInput" value={this.state.taskToEdit.date} onChangeText={(text) => this.updateTaskToEdit("date", text)} name="taskDate" />
                                        <Text>Task comments</Text>
                                        <TextInput type="TextInput" value={this.state.taskToEdit.comments} onChangeText={(text) => this.updateTaskToEdit("comments", text)} name="taskComments" />
                                        <Button title="Save" color="lime" onPress={() => this.addTask()} />
                                        <Button title="Delete" color="red" onPress={() => this.deleteTask(this.state.taskToEdit.id)} />
                                    </Form>
                                </View>
                            ) : (
                                    <View>
                                        <Form ref="form">
                                            <TextInput type="TextInput" placeholder="Task name" name="taskName" />
                                            <TextInput type="TextInput" placeholder="Task date" name="taskDate" />
                                            <TextInput type="TextInput" placeholder="Task comments" name="taskComments" />
                                            <Button title="Save" backgroundColor="blue" onPress={() => this.addTask()} />
                                        </Form>
                                    </View>
                                )
                            }
                        </Overlay>
                </ScrollView>
            </SafeAreaView>
       );
    }
}
export default HomeScreen;


const styles = StyleSheet.create({
    newButton: { backgroundColor: 'red', marginRight: 10, marginBottom: 15 },
    droidSafeArea: {
        flex: 1,
        backgroundColor: 'grey',
        paddingTop: Platform.OS === 'android' ? 25 : 0
    }
});