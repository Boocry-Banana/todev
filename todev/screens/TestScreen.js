import React, { Component } from 'react';
import {Button, FlatList, StyleSheet, Text, View, Image, AsyncStorage } from 'react-native';
import Loading from '../components/Loading';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { init } from '../redux/actions/tasksActions';
import ItemTask from '../components/ItemTask';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

class TestScreen extends Component {

    state = { data: null };

    getPermission = async ()=>{

        let result = await Permissions.askAsync(Permissions.LOCATION);

        if (Constants.isDevice && result.status != 'granted') {
            alert('Location permissions is not granted.')
        }
        else{
            console.log("location")
            const location = await Location.getCurrentPositionAsync()
            const res = await this.props.serv.getJourneyService("fr-idf", location.coords.longitude, location.coords.latitude, "2.264357", "48.891408", "20200117T162957")
            // console.log("res")
            // console.log(res)
            
            this.setState({ location: location, journey: res})
           
        }
    }

    componentDidMount() {
    
        // var json = require('./../tasks.json');
        // tasks = JSON.stringify(json);
        this.getTasks();
        // this.saveTasks(json.tasks)
        // this.saveTasks(json.tasks)
        // this.saveTasks(json.tasks)
        // this.saveTasks(json.tasks)
        // this.setState({data: json})
        //
        // console.log(FlatList, 'flatslist');
        // console.log(ItemTask, 'itetask');
        // console.log('111111111111111111111111111111');
        // console.log(json);
        // console.log('222222222222222222222222222222');
        // console.log(json.tasks);
        // console.log('3333333333333333333333333333333');
        // console.log('4646464646'+this.state.tasks);
    
        // const t = this.serv.getLastTasks("", "").then(resp => {
        //     this.setState({data: resp})
        //     console.log(this.state.data)
        // })
    
        // this.props.serv.getWeatherByCity('nanterre').then(resp => {
        //     this.setState({ data: resp.data });
        // });

        // this.props.actionInit();
        // console.log(this.props)
        this.getPermission()
    }    


    saveTasks = async (tasks) => {
        // tasks = this.state.tasks
        if (tasks != null) {
            let data = await AsyncStorage.getItem('TASKS');
            let tab = [];
            if (data != null) {
                tab = JSON.parse(data);
            }

            tab = tab.concat(tasks);
            
            console.log(tab);
            await AsyncStorage.setItem('TASKS', JSON.stringify(tab));
            //this.props.navigation.goBack();
        } else {
            alert(`Error n save`);
        }
    }




    
    getTasks = async () => {
        let data = await AsyncStorage.getItem('TASKS');
        let tab = [];
        if (data != null) {
            tab = JSON.parse(data);
        }
        console.log('464646464646'+tab);
        this.setState({tasks: tab})
    }
    // _displayDetailForTask = (task) => {
    //     console.log(this.props)
    //     this.props.navigation.navigate("DetailTask", { task: task })
    // }

    render() {
        if (this.state.journey){
            console.log("this.state")
            console.log(this.state.journey)
            // const journey = this.state.journey.map((data) => {
            //     return <Text>test</Text>
            //     })
            const {sections} = this.state.journey
            return sections.map((data) => {
                return (
                    <View><Text>{data.type}</Text></View>
                )
            })
    }
    else{
        return(<Text> Plop</Text>)
    }
    }
}

const mapStateToProps = (stateStore) => {
    // console.log('store', stateStore);
    return {
        serv: stateStore.services.journeyService,
        fav: stateStore.tasks
    };
};

const mapActionsToProps = (payload) => {
    return {
        actionInit: bindActionCreators(init, payload)
    };
};

export default connect(mapStateToProps, mapActionsToProps)(TestScreen);
// export default HomeScreen;

const Sunrise = (props) => {
    const dt = new Date(props.time * 1000);
    const result = `${dt.getHours()}:${dt.getMinutes()}`;
    return (<Text>Levé: {result}</Text>);
}

const Sunset = (props) => {
    const dt = new Date(props.time * 1000);
    const result = `${dt.getHours()}:${dt.getMinutes()}`;
    return (<Text>Couché: {result}</Text>);
}

const styles = StyleSheet.create({
    Clear: { backgroundColor: 'blue' },
    Sunny: { backgroundColor: 'yellow' },
    Drizzle: { backgroundColor: 'grey' },
    newButton: { backgroundColor: 'red', marginRight: 10, marginBottom: 15 },
});