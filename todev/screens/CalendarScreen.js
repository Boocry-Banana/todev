import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, AsyncStorage, SafeAreaView, StatusBar, Dimensions, Platform, ScrollView } from 'react-native';
import Loading from '../components/Loading';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { init } from '../redux/actions/tasksActions';
import DateTime from 'react-native-customize-selected-date'
import _ from 'lodash'

class CalendarScreen extends Component {

    state = { data: null };
    constructor(props) {
        super(props)
        this.state = {
          time: '',
          tasks: null,
          eventsDate: null,
          dateToDisplay: "",
          eventTitle: "",
          eventInfo: ""
        }
      }
    
      componentDidMount() {
        this.getTasks();
    }    

    componentDidUpdate() {
        this.getTasks();
    }

    loadEventToDisplay(date) {

        let tempEventsDate = []
        this.getTasks()

        this.state.tasks.forEach( event => {
            let dateToFormat = event.date.split("\/")
            let dayFormat = dateToFormat[0]
            let monthFormat = dateToFormat[1]
            let yearFormat = dateToFormat[3]
            let eventDate = yearFormat + "-" + monthFormat + "-" + dayFormat
            tempEventsDate.push(eventDate)

            if (_.includes([eventDate], date)) {
                return <Image source={require('../Images/Icon/task_white.png')} style={styles.icLockRed} />
            }
            
        });
        this.setState({eventsDate: tempEventsDate})
    }

    updateView(event, dateToCheck) {

        let day = dateToCheck[2]
        let month = dateToCheck[1]
        let year = dateToCheck[0]

        let date = day + "\/" + month + "\/" + year
        if(event.date == date) {
            this.setState({
                dateToDisplay: event.date,
                eventTitle: event.name,
                eventInfo: event.comments
            })
        } else {
            this.setState({
                dateToDisplay: "No event",
                eventTitle: "",
                eventInfo: ""
            })
        }

        
    }
      onChangeDate(date) {
        
        let dateToCheck = date.split("-")
        this.getTasks()

        this.state.tasks.forEach( event =>
            this.updateView(event, dateToCheck)
        );
      }
    
      renderChildDay(day) {
          
        //this.loadEventToDisplay()
        if (_.includes(["2020-01-24"], day)) {
            return <Image source={require('../Images/Icon/task_white.png')} style={styles.icLockRed} />
        }
      }
    
      getTasks = async () => {
        let data = await AsyncStorage.getItem('TASKS');
        let tab = [];
        if (data != null) {
            tab = JSON.parse(data);
        }
        this.setState({tasks: tab})
    }

      render() {
    
        const screenWidth = Math.round(Dimensions.get('window').width);
        const screenHeight = Math.round(Dimensions.get('window').height);

        return (
          <SafeAreaView style={[styles.container, styles.droidSafeArea]}>
            <View style={styles.calendarView}>
              <View style={{flex: 0.1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: screenWidth*0.1, fontWeight: 'bold', paddingTop: screenHeight*0.05}}>Upcoming Events</Text>
              </View>
              <DateTime
                  date={this.state.time}
                  changeDate={(date) => this.onChangeDate(date)}
                  format='YYYY-MM-DD'
                  renderChildDay={(day) => this.renderChildDay(day)}
                />
              <ScrollView>
              <View style={[styles.container, {flexDirection: "column", alignItems: "center"}]} >
                <Text style={{paddingTop: screenHeight*0.05}}>{this.state.eventTitle}</Text>
                <Text style={{paddingTop: screenHeight*0.05}}>{this.state.dateToDisplay}</Text>
                <Text style={{paddingTop: screenHeight*0.05, paddingLeft: screenWidth*0.04}}>{this.state.eventInfo}</Text>
              </View>
              </ScrollView>
            </View>
          </SafeAreaView>
        );
      }
}

const mapStateToProps = (stateStore) => {
    // console.log('store', stateStore);
    return {
        serv: stateStore.services.journeyService,
        task: stateStore.tasks
    };
};

const mapActionsToProps = (payload) => {
    return {
        actionInit: bindActionCreators(init, payload)
    };
};

export default connect(mapStateToProps, mapActionsToProps)(CalendarScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
      },
      calendarView: {
        flex:1,
        backgroundColor: 'grey'
      },
      taskInfos: {
        flex:4,
        backgroundColor: 'grey'
      },
      icLockRed: {
        width: 13 / 2,
        height: 9,
        position: 'absolute',
        top: 2,
        left: 1
      },
      droidSafeArea: {
        backgroundColor: 'black' ,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
      }
});